import Vue from 'vue'
import firebase from 'firebase'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';

Vue.config.productionTip = false

const firebaseConfig = {
    apiKey: "AIzaSyD5YPAHHwmVwiqgZ2OOrLRhM1cY7HkBNmw",
    authDomain: "inncosys-slider.firebaseapp.com",
    databaseURL: "https://inncosys-slider.firebaseio.com",
    projectId: "inncosys-slider",
    storageBucket: "inncosys-slider.appspot.com",
    messagingSenderId: "110769280188",
    appId: "1:110769280188:web:f01a3ffb373219c38b2cb9"
};

firebase.initializeApp(firebaseConfig);

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
