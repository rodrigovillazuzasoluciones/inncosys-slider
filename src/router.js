import Vue 		from 'vue'
import Router 	from 'vue-router'
import AddImage from './components/AddImage.vue';
import Carousel from './components/Carousel.vue'; 

Vue.use(Router)

export default new Router({
  routes: [
  	{path:'', 			component: Carousel,	name:'Carousel'},
  	{path:'/agregar', 	component: AddImage,	name:'Agregar_Foto'},
  ]
})
