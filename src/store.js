import Vue from 'vue'
import Vuex from 'vuex'
import firebase from 'firebase';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  	db:null
  },
  mutations: {
  	db_firebase(state,db){
  		state.db=firebase.firestore()
  	}
  },
  actions: {
  	definir_db_firebase({commit,state}){
  		commit('db_firebase');
  	}
  },
  getters:{
  	get_db_firebase(state){
       return state.db;
    },
  }
})
